from find_island import find_island

import numpy as np
import unittest


LAND_1, RES_1 = np.array([[1, 1, 0, 1, 0],
                          [0, 1, 1, 1, 0],
                          [0, 0, 0, 0, 0]]), 1

LAND_2, RES_2 = np.array([[1, 0, 0, 0, 0],
                          [0, 1, 1, 1, 0],
                          [0, 0, 0, 0, 0]]), 2

LAND_3, RES_3 = np.array([[1, 0, 0, 0, 1],
                          [0, 1, 1, 1, 0],
                          [0, 0, 0, 1, 0]]), 3

LAND_4, RES_4 = np.array([[1, 1, 1],
                          [1, 1, 1]]), 1

LAND_5, RES_5 = np.array([[0, 0, 0],
                          [0, 0, 0]]), 0

LAND_6, RES_6 = np.array([[1, 0, 0, 0, 1],
                          [0, 1, 1, 1, 0],
                          [0, 0, 0, 0, 0],
                          [0, 0, 0, 0, 1],
                          [0, 1, 1, 0, 0],
                          [0, 0, 0, 1, 0]]), 6


class TestFindIsland(unittest.TestCase):
    def test_1(self):
        self.assertEquals(RES_1, find_island(LAND_1))

    def test_2(self):
        self.assertEquals(RES_2, find_island(LAND_2))

    def test_3(self):
        self.assertEquals(RES_3, find_island(LAND_3))

    def test_4(self):
        self.assertEquals(RES_4, find_island(LAND_4))

    def test_5(self):
        self.assertEquals(RES_5, find_island(LAND_5))

    def test_6(self):
        self.assertEquals(RES_6, find_island(LAND_6))


if __name__ == "__main__":
    unittest.main()