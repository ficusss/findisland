"""
This module contains a function for finding islands in a planar matrix.
"""
import numpy as np

FREE_ISLAND = 1
BUSY_ISLAND = 2


def breadth_first_search(land: np.ndarray, start_point: tuple) -> None:
    """
    Width search function.
    :param land: planar matrix
    :param start_point: start point on current island
    :return: None
    """
    def _within_land(_point: tuple) -> bool:
        return 0 <= _point[0] < land.shape[0] \
               and 0 <= _point[1] < land.shape[1]

    def _is_island(_point: tuple) -> bool:
        return land[_point] == FREE_ISLAND

    curr_points = [start_point]
    for point in curr_points:
        land[point] = BUSY_ISLAND
        adj_points = [(point[0], (point[1]-1)), ((point[0]+1), point[1]),
                      (point[0], (point[1]+1)), ((point[0]-1), point[1])]
        curr_points.extend([p for p in adj_points
                            if _within_land(p) and _is_island(p)])


def find_island(land: np.ndarray) -> int:
    """
    Search for islands in a planar matrix.
    :param land: planar matrix
    :return: count islands
    """
    n_islands = 0
    while np.any(land == FREE_ISLAND):
        n_islands += 1
        index = np.where(land == FREE_ISLAND)
        breadth_first_search(land, (index[0][0], index[1][0]))
    return n_islands
